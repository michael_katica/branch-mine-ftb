doNotCollect= {"minecraft:dirt", "minecraft:stone", "minecraft:gravel"}

function checkfuel()
	if turtle.getFuelLevel() < 250 then
		turtle.select(1)
		turtle.refuel(8)
	end
end

function contains(list, search)
	iter = list_iter(list)
	element = iter()
	while element do
		if element == search then
			return true
		end
		element = iter()
	end
	return false
end

function findSlot(direction)
	local value = 2
	local placed = false
	turtle.select(value)
	while value < 17 and placed == false do
		if (turtle.compare() and turtle.getItemSpace() > 0) or turtle.getItemCount == 0 then
			placed = true
			if direction == 1 then
				turtle.suck()
			end
			if direction == 2 then
				turtle.suckUp()
			end
		end
		value = value + 1
	end
end

function mineOneForward()
	local level = 1
	local success, nextBlock = turtle.inspect()
	while level < 3 do
		while turtle.detect() do
			success, nextBlock = turtle.inspect()
			turtle.dig()
			if success and not contains(doNotCollect, nextBlock.name) then
				findSlot(1)
			end
		end
		turtle.forward()
		turtle.turnLeft()
		while turtle.detect() do
			success, nextBlock = turtle.inspect()
			turtle.dig()
			if success and not contains(doNotCollect, nextBlock.name) then
				findSlot(1)
			end
		end
		turtle.turnRight()
		turtle.turnRight()
		while turtle.detect() do
			success, nextBlock = turtle.inspect()
			turtle.dig()
			if success and not contains(doNotCollect, nextBlock.name) then
				findSlot(1)
			end
		end
		turtle.turnLeft()
		while turtle.detectUp() do
			success, nextBlock = turtle.inspectUp()
			turtle.digUp()
			if success and not contains(doNotCollect, nextBlock.name) then
				findSlot(2)
			end
		end
		if turtle.up() then
			level = level + 1
		end
	end
	while level > 1 do
		if turtle.down() then
			level = level + 1
		end
	end
end

print "Running"
mineOneForward()
